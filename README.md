## BatchDeposit v2

Efficiently batch multiple deposits into a single transaction to
reduce gas costs.

Ready for Pectra.

### Deployments

- [mainnet](0xDc8D2A1d8cd79e1542441743e448129a00A59aE0)
- [holesky](0x273Ead630d8E105eb9fA479c2A8B39ABb8D83059)

### Interface

```
interface IBatchDeposit2 {
    function batchDeposit(
        uint256 deadline, 
        uint256[] calldata values, 
        bytes calldata args
    ) external payable;
}
```

### Inputs

```
deadline:
  A block number after which the transaction will fail.

values:
  An array of deposit amounts (in wei) for each distinct deposit.

args:
  A flat byte string containing validator details for each deposit.
  Each element is 208 bytes long and is composed of the following:
    - pubkey                 (48 bytes)
    - withdrawal_credentials (32 bytes)
    - signature              (96 bytes)
    - deposit_data_root      (32 bytes)
```

Here's how a batch deposit would look for two validators with the following details:

```JSON
[{
    "pubkey": "0x8a87642d46236dba6ffb1255ee862b2160a3f8cc9ecaad3806de250d533cea7ea035e140b32d06e736391a7a7855ff31",
    "withdrawalCredentials": "0x01000000000000000000000090f8bf6a479f320ead074411a4b0e7944ea8c9c1",
    "signature": "0xa501d76863212170b149e065667040f5a3a858e70d70a1c8c0f0c84d618d5a28e7b7890be0f4ddf190182b086010ed4917f4c59f1f556849a798ab57770e1a8d306381a5dec7dd022f22b1773dd47e6c106fcf4edb0ce54f110e9596db8ea283",
    "depositDataRoot": "0xb8988edd47493b19029bc82bff0d4ab252120d37459484671cbd7c8fc2902102",
},
{
    "pubkey": "0xb5f882d09873c5a4e380a2e61d626d1548585cd85f803678fcca2612310dc5841e92fa709382fa57189b13b351515961",
    "withdrawalCredentials": "0x01000000000000000000000090f8bf6a479f320ead074411a4b0e7944ea8c9c1",
    "signature": "0xa40375aa1239fb919e0a1c9b4ede58ca3734bb400d53dfee646ef5b3e7ae4fbea0b76064c3feb8583410810412d1793318af0dc78ef6548a777c85a14e954b52c6116aa863ae67b7a7cea0b4a13089f898131d556a9a9b42b4c6d8c6b89f65ce",
    "depositDataRoot": "0xe71738ffd0545bfa44b86e29cc43b22aaca8a2fe262b2b908769ad7a66b09141",
}]
```

```
uint256[] memory values = new uint256[](2);
values[0] = 32 ether;
values[1] = 32 ether;

bytes memory data = abi.encodePacked(
    hex"8a87642d46236dba6ffb1255ee862b2160a3f8cc9ecaad3806de250d533cea7ea035e140b32d06e736391a7a7855ff31",
    hex"01000000000000000000000090f8bf6a479f320ead074411a4b0e7944ea8c9c1",
    hex"a501d76863212170b149e065667040f5a3a858e70d70a1c8c0f0c84d618d5a28e7b7890be0f4ddf190182b086010ed4917f4c59f1f556849a798ab57770e1a8d306381a5dec7dd022f22b1773dd47e6c106fcf4edb0ce54f110e9596db8ea283",
    hex"b8988edd47493b19029bc82bff0d4ab252120d37459484671cbd7c8fc2902102",

    hex"b5f882d09873c5a4e380a2e61d626d1548585cd85f803678fcca2612310dc5841e92fa709382fa57189b13b351515961",
    hex"01000000000000000000000090f8bf6a479f320ead074411a4b0e7944ea8c9c1",
    hex"a40375aa1239fb919e0a1c9b4ede58ca3734bb400d53dfee646ef5b3e7ae4fbea0b76064c3feb8583410810412d1793318af0dc78ef6548a777c85a14e954b52c6116aa863ae67b7a7cea0b4a13089f898131d556a9a9b42b4c6d8c6b89f65ce",
    hex"e71738ffd0545bfa44b86e29cc43b22aaca8a2fe262b2b908769ad7a66b09141"
);

c.batchDeposit{value: 64 ether}(
    block.number,
    values,
    data
);
```

### How does it work?

The batch deposit contract processes each deposit individually by
manually constructing the `DepositContract.deposit()` calldata.  It
extracts the relevant information from the `args` byte array for each
deposit.

Below is an example of what the deposit calldata looks like:

```
| offset | value                                                            | description        |
|--------+------------------------------------------------------------------+--------------------|
|   0x00 | 0x22895118                                                       | deposit() selector |
|   0x04 | 0000000000000000000000000000000000000000000000000000000000000080 | pubkey offset      |
|   0x24 | 00000000000000000000000000000000000000000000000000000000000000e0 | credentials offset |
|   0x44 | 0000000000000000000000000000000000000000000000000000000000000120 | signature offset   |
|   0x64 | dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd | deposit_data_root  |
|   0x84 | 0000000000000000000000000000000000000000000000000000000000000030 | pubkey length      |
|   0xa4 | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | pubkey             |
|   0xc4 | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa00000000000000000000000000000000 | pubkey             |
|   0xe4 | 0000000000000000000000000000000000000000000000000000000000000020 | credentials length |
|  0x104 | bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb | credentials        |
|  0x124 | 0000000000000000000000000000000000000000000000000000000000000060 | signature length   |
|  0x144 | cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc | signature          |
|  0x164 | cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc | signature          |
|  0x184 | cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc | signature          |
|  0x1a4 |                                                                  | end                |

where:
  0xaa...aa is the pubkey
  0xbb...bb is the withdrawal_credentials
  0xcc...cc is the signature
  0xdd...dd is the deposit_data_root
```

Using gas-optimized assembly, those parts are replaced with the
provided validator data, and a deposit is then made.

### Gas comparison

```
| # deposits |    1by1 |      v1 |      v2 |
|------------+---------+---------+---------|
|          1 |   50526 |   61151 |   61115 |
|         10 |  523998 |  333178 |  325576 |
|        100 | 5098217 | 2854391 | 2770997 |

where:
  - 1by1 denotes a single transaction per deposit
  - v1 is the first version of the batch deposit contract
  - v2 is this version
```

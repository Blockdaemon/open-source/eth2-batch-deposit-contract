// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Script, console} from "forge-std/Script.sol";
import {BatchDeposit2} from "../src/BatchDeposit2.sol";

contract BatchDeposit2Script is Script {
    BatchDeposit2 public target;

    function setUp() public {}

    function run() public {
        // address DEPOSIT_CONTRACT = 0x00000000219ab540356cBB839Cbe05303d7705Fa;
        address DEPOSIT_CONTRACT = 0x4242424242424242424242424242424242424242;

        vm.startBroadcast();

        target = new BatchDeposit2(DEPOSIT_CONTRACT);

        vm.stopBroadcast();
    }
}

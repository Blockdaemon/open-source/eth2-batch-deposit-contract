// SPDX-License-Identifier: GPL-3.0-or-later

pragma solidity 0.8.28;

interface IBatchDeposit2 {
    receive() external payable;

    fallback() external payable;

    function batchDeposit(uint256 deadline, uint256[] calldata values, bytes calldata args) external payable;
}

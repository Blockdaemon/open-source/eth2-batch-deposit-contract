import * as fs from "fs";

import "@nomicfoundation/hardhat-toolbox";
import hre from "hardhat";
import type { HardhatRuntimeEnvironment } from "hardhat/types";
import type {
    BaseContract,
    ConstantContractMethod,
    ContractFactory,
    ContractTransactionReceipt,
    ContractTransactionResponse,
    Interface,
    Signer,
} from "ethers";
import data, { DepositArgs } from "../src/data";
import { IDepositContract } from "../typechain-types";

const DEPOSIT_CONTRACT: string = "0x00000000219ab540356cBB839Cbe05303d7705Fa";

interface IBatchDeposit2 /*extends ContractInterface*/ {
    // batchDeposit: ConstantContractMethod<[bigint, string], ContractTransactionResponse>;
    batchDeposit: ConstantContractMethod<[bigint, Array<bigint>, string], ContractTransactionResponse>;
}

type BatchDeposit = BaseContract & {
    deploymentTransaction(): ContractTransactionResponse;
} & Omit<IBatchDeposit2, keyof BaseContract>;

async function deploy(
    hre: HardhatRuntimeEnvironment,
    deployer: Signer,
    path: string,
): Promise<BatchDeposit> {
    const artefacts: {
        abi: Interface;
        bytecode: string;
    } = JSON.parse(
        fs.readFileSync(path, "utf8")
    );
    const factory: ContractFactory<[string], IBatchDeposit2> =
          new hre.ethers.ContractFactory(artefacts.abi, artefacts.bytecode, deployer);
    const target: BatchDeposit = await factory.deploy(DEPOSIT_CONTRACT);
    return target.waitForDeployment();
}

async function makeDeadline(): Promise<bigint> {
    return await hre.ethers.provider.getBlockNumber().then(x => BigInt(x) + 16n);
}

function makeValues(n: number): [bigint, Array<bigint>] {
    return [
        hre.ethers.parseEther("32") * BigInt(n),
        new Array<bigint>(n).fill(hre.ethers.parseEther("32")),
    ];
}

function makeDepositData(n: number): string {
    return "0x" + data.slice(0, n).map(
        (args: DepositArgs): string => (
            args.pubkey.slice(2) +
                args.withdrawalCredentials.slice(2) +
                args.signature.slice(2) +
                args.depositDataRoot.slice(2)
            // '0' + args.depositDataRoot.slice(3)
        )
    ).join("");
}

async function depositSingle(contract: IDepositContract, id: number): Promise<bigint> {
    const tx: ContractTransactionResponse = await contract.deposit(
        data[0].pubkey,
        data[0].withdrawalCredentials,
        data[0].signature,
        data[0].depositDataRoot,
        { value: hre.ethers.parseEther("32") },
    );
    const receipt: ContractTransactionReceipt | null = await tx.wait();
    if (receipt == null) {
        throw new Error();
    }
    return receipt.gasUsed;
}

async function main() {
    const RUNS: number = 1;

    // const deposit: IDepositContract = await hre.ethers.getContractAt("IDepositContract", DEPOSIT_CONTRACT);
    // let sum: bigint = 0n;
    // for (let i: number = 0; i < RUNS; i++) {
    //     sum += await depositSingle(deposit, i);
    // }
    // console.log("TOTAL:", sum);
    // if (1 == 1) {return;}

    const [deployer]: Signer[] = await hre.ethers.getSigners();
    // const path: string = "../../out/BatchDeposit.sol/BatchDeposit.json";
    const path: string = "../../out/BatchDeposit2.sol/BatchDeposit2.json";
    const batch: BatchDeposit = await deploy(hre, deployer, path);
    const deadline: bigint = await makeDeadline();
    // const deadline: bigint = BigInt(Math.floor(Date.now() / 1000) + 60);
    const [total, values]: [bigint, bigint[]] = makeValues(RUNS);
    const tx: ContractTransactionResponse = await batch.batchDeposit(
        deadline,
        values,
        makeDepositData(RUNS),
        { value: total },
    );
    const receipt: ContractTransactionReceipt | null = await tx.wait();
    if (receipt == null) {
        throw new Error();
    }
    console.log(`path: ${path}`);
    console.log(`runs: ${RUNS}`);
    console.log(`gasUsed: ${receipt.gasUsed}`);
}

main().catch(console.error);

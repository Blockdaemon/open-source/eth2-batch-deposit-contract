import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";

const { BLOCK_NUMBER, FOUNDRY_ETH_RPC_URL } = process.env;

if (FOUNDRY_ETH_RPC_URL == null) {
    throw new Error();
}

const config: HardhatUserConfig = {
    networks: {
        hardhat: {
            forking: {
                enabled: true,
                url: FOUNDRY_ETH_RPC_URL,
                blockNumber: Number(BLOCK_NUMBER ?? 21822900),
            },
            accounts: [{
                privateKey: "ac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80",
                balance: "480582401619720537646656920202073836305075619950168356609076886751592579072",
            }],
        },
    },
    solidity: {
        version: "0.8.28",
    }
};

export default config;

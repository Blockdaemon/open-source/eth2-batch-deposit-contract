#!/bin/bash

MAINNET=0x00000000219ab540356cBB839Cbe05303d7705Fa
HOLESKY=0x4242424242424242424242424242424242424242

forge create                                  \
      --broadcast                             \
      --etherscan-api-key "$ETHERSCAN_APIKEY" \
      --private-key "$MNEMONIC"               \
      --verify                                \
      src/BatchDeposit2.sol:BatchDeposit2     \
      --constructor-args "$HOLESKY"

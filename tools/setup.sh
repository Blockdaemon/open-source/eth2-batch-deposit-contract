#!/bin/bash

# forge install

# Prepare Mythril
rm -rf env_m
python3 -m venv env_m
source env_m/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install mythril
deactivate

# Install solc for Mythril
curl -O 'https://github.com/ethereum/solidity/releases/download/v0.8.28/solc-static-linux'
[ "$(md5sum solc-static-linux)" = 'd41d8cd98f00b204e9800998ecf8427e  solc-static-linux' ] || exit 1
chmod a+x solc-static-linux
mv solc-static-linux env_m/bin/solc

# Prepare Slither
rm -rf env_s
python3 -m venv env_s
source env_s/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install slither-analyzer
deactivate

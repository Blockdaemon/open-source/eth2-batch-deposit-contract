#!/bin/bash

ROOT=$(dirname "$(dirname "$(realpath "$0")")")

cd "$ROOT" || exit
forge build

cd "$ROOT"/tools/benchmark || exit
if [ ! -d node_modules ] ; then
    npm install
fi
npx hardhat --network hardhat run scripts/benchmark.ts

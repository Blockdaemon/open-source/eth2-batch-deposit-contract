#!/bin/bash

forge test                                                                     \
      --fork-block-number "$BLOCK_NUMBER"                                      \
      --fork-url "$FOUNDRY_ETH_RPC_URL"                                        \
      -vv                                                                      \
      "$@"

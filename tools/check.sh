#!/bin/bash

ROOT=$(dirname "$(dirname "$(realpath "$0")")")


echo '##############'
echo '# Solhint'
echo '##############'
echo
(
    cd "$ROOT"/tools/linters || exit
    if [ ! -d node_modules ] ; then
        npm install
    fi

    npx solhint "$ROOT"/src/BatchDeposit2.sol
)


echo
echo
echo '##############'
echo '# Slither'
echo '##############'
echo
source env_s/bin/activate
slither src/BatchDeposit2.sol
deactivate


echo
echo
echo '##############'
echo '# Mythril'
echo '##############'
echo
source env_m/bin/activate
myth analyze src/BatchDeposit2.sol
deactivate

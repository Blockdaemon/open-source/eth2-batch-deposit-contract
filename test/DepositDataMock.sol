// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

library DepositDataMock {
    function makePubkey(uint8 id) public pure returns (bytes memory) {
        bytes1 x;
        unchecked {
            x = bytes1(0x31 + id);
        }
        bytes memory pubkey = new bytes(0x30);
        for (uint256 i = 0; i < pubkey.length; i++) {
            pubkey[i] = x;
        }
        return pubkey;
    }

    function makeWithdrawalCredentials(uint8 id) public pure returns (bytes memory) {
        bytes1 x;
        unchecked {
            x = bytes1(0x41 + id);
        }
        bytes memory withdrawalCredentials = new bytes(0x20);
        for (uint256 i = 0; i < withdrawalCredentials.length; i++) {
            withdrawalCredentials[i] = x;
        }
        return withdrawalCredentials;
    }

    function makeSignature(uint8 id) public pure returns (bytes memory) {
        bytes1 x;
        unchecked {
            x = bytes1(0x51 + id);
        }
        bytes memory signature = new bytes(0x60);
        for (uint256 i = 0; i < signature.length; i++) {
            signature[i] = x;
        }
        return signature;
    }

    function makeDepositDataRoot(uint8 id) public pure returns (bytes32 result) {
        bytes1 x;
        unchecked {
            x = bytes1(0x61 + id);
        }
        bytes memory s = new bytes(32);
        for (uint256 i = 0; i < 32; i++) {
            s[i] = x;
        }
        assembly {
            result := mload(add(s, 0x20))
        }
    }

    function makeCalldata(uint8 id) public pure returns (bytes memory) {
        // | offset |                                                            value | description                        |
        // |--------+------------------------------------------------------------------+------------------------------------|
        // |   0x00 |                                                       0x22895118 | selector                           |
        // |   0x04 | 0000000000000000000000000000000000000000000000000000000000000080 | pubkey.offset                      |
        // |   0x24 | 00000000000000000000000000000000000000000000000000000000000000e0 | withdrawal_credentials.offset      |
        // |   0x44 | 0000000000000000000000000000000000000000000000000000000000000120 | signature.offset                   |
        // |   0x64 | dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd | deposit_data_root                  |
        // |   0x84 | 0000000000000000000000000000000000000000000000000000000000000030 | pubkey.length (48)                 |
        // |   0xa4 | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | pubkey                             |
        // |   0xc4 | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa00000000000000000000000000000000 | pubkey                             |
        // |   0xe4 | 0000000000000000000000000000000000000000000000000000000000000020 | withdrawal_credentials.length (32) |
        // |  0x104 | bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb | withdrawal_credentials             |
        // |  0x124 | 0000000000000000000000000000000000000000000000000000000000000060 | signature.length (96)              |
        // |  0x144 | cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc | signature                          |
        // |  0x164 | cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc | signature                          |
        // |  0x184 | cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc | signature                          |
        // |  0x1a4 |                                                                  | end                                |
        bytes memory pubkey = makePubkey(id);
        bytes memory withdrawalCredentials = makeWithdrawalCredentials(id);
        bytes memory signature = makeSignature(id);
        bytes32 depositDataRoot = makeDepositDataRoot(id);
        bytes memory data = abi.encodeWithSignature(
            "deposit(bytes,bytes,bytes,bytes32)", pubkey, withdrawalCredentials, signature, depositDataRoot
        );
        return data;
    }

    function make(uint8 id) public pure returns (bytes memory) {
        bytes memory pubkey = makePubkey(id);
        bytes memory withdrawalCredentials = makeWithdrawalCredentials(id);
        bytes memory signature = makeSignature(id);
        bytes32 depositDataRoot = makeDepositDataRoot(id);
        return abi.encodePacked(pubkey, withdrawalCredentials, signature, depositDataRoot);
    }
}

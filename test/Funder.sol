// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

contract Funder {
    address payable immutable target;

    constructor(address payable _target) payable {
        require(msg.value > 0);
        target = _target;
    }

    function fund() external {
        selfdestruct(target);
    }
}

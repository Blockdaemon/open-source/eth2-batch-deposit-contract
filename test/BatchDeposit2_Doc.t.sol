// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

import {Test, console} from "forge-std/Test.sol";

import {BatchDeposit2} from "../src/BatchDeposit2.sol";
import {IDepositContract} from "./interfaces/IDepositContract.sol";

import {DepositData} from "./DepositData.sol";

contract BatchDeposit2_Deposit_Test is Test {
    address constant DEPOSIT_CONTRACT = 0x00000000219ab540356cBB839Cbe05303d7705Fa;

    IDepositContract depositContract;
    BatchDeposit2 private target;
    DepositData private provider;

    function setUp() public {
        // Ensure unique deployment addresses.
        vm.startPrank(0xd380139FFe8a712B7c55F5f9AEe1603d56732a9c);
        depositContract = IDepositContract(DEPOSIT_CONTRACT);
        target = new BatchDeposit2(DEPOSIT_CONTRACT);
        provider = new DepositData();
        vm.stopPrank();
    }

    function test_Example() public {
        uint256[] memory values = new uint256[](2);
        values[0] = 32 ether;
        values[1] = 32 ether;

        bytes memory data = abi.encodePacked(
            hex"8a87642d46236dba6ffb1255ee862b2160a3f8cc9ecaad3806de250d533cea7ea035e140b32d06e736391a7a7855ff31",
            hex"01000000000000000000000090f8bf6a479f320ead074411a4b0e7944ea8c9c1",
            hex"a501d76863212170b149e065667040f5a3a858e70d70a1c8c0f0c84d618d5a28e7b7890be0f4ddf190182b086010ed4917f4c59f1f556849a798ab57770e1a8d306381a5dec7dd022f22b1773dd47e6c106fcf4edb0ce54f110e9596db8ea283",
            hex"b8988edd47493b19029bc82bff0d4ab252120d37459484671cbd7c8fc2902102",
            hex"b5f882d09873c5a4e380a2e61d626d1548585cd85f803678fcca2612310dc5841e92fa709382fa57189b13b351515961",
            hex"01000000000000000000000090f8bf6a479f320ead074411a4b0e7944ea8c9c1",
            hex"a40375aa1239fb919e0a1c9b4ede58ca3734bb400d53dfee646ef5b3e7ae4fbea0b76064c3feb8583410810412d1793318af0dc78ef6548a777c85a14e954b52c6116aa863ae67b7a7cea0b4a13089f898131d556a9a9b42b4c6d8c6b89f65ce",
            hex"e71738ffd0545bfa44b86e29cc43b22aaca8a2fe262b2b908769ad7a66b09141"
        );

        target.batchDeposit{value: 64 ether}(block.number, values, data);
    }
}

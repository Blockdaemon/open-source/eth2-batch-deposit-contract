// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

import {Test, console} from "forge-std/Test.sol";

import {BatchDeposit2} from "../src/BatchDeposit2.sol";

contract BatchDeposit2_Balance_Test is Test {
    address constant DEPOSIT_CONTRACT = 0x00000000219ab540356cBB839Cbe05303d7705Fa;

    BatchDeposit2 private target;

    function setUp() public {
        // Ensure unique deployment addresses.
        vm.startPrank(0xd380139FFe8a712B7c55F5f9AEe1603d56732a9c);
        target = new BatchDeposit2(DEPOSIT_CONTRACT);
        vm.stopPrank();
    }

    function test_Balance_Call_1() public {
        vm.expectRevert(BatchDeposit2.FundingNotAccepted.selector, address(target));
        (bool success,) = payable(address(target)).call{value: 1 wei}("");
        vm.assertEq(success, success); // silence unused warning
        vm.assertEq(address(target).balance, 0);
    }

    function test_Balance_Call_2() public {
        (bool success,) = payable(address(target)).call{value: 1 wei}("");
        vm.assertFalse(success);
        vm.assertEq(address(target).balance, 0);
    }

    function test_Balance_Send_1() public {
        vm.expectRevert(BatchDeposit2.FundingNotAccepted.selector, address(target));
        bool success = payable(address(target)).send(1 wei);
        vm.assertEq(success, success); // silence unused warning
        vm.assertEq(address(target).balance, 0);
    }

    function test_Balance_Send_2() public {
        bool success = payable(address(target)).send(1 wei);
        vm.assertFalse(success);
        vm.assertEq(address(target).balance, 0);
    }

    function test_Balance_Transfer() public {
        vm.expectRevert(BatchDeposit2.FundingNotAccepted.selector, address(target));
        payable(address(target)).transfer(1 wei);
        vm.assertEq(address(target).balance, 0);
    }
}

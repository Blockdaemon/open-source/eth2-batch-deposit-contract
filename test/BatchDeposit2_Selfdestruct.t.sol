// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

import {Test, console} from "forge-std/Test.sol";

import {BatchDeposit2} from "../src/BatchDeposit2.sol";
import {IDepositContract} from "./interfaces/IDepositContract.sol";

import {DepositData} from "./DepositData.sol";
import {Funder} from "./Funder.sol";

contract BatchDeposit2_Selfdestruct_Test is Test {
    address constant DEPOSIT_CONTRACT = 0x00000000219ab540356cBB839Cbe05303d7705Fa;

    IDepositContract private depositContract;
    BatchDeposit2 private target;
    DepositData private provider;
    Funder private funder;

    function setUp() public {
        address DEPLOYER = 0xd380139FFe8a712B7c55F5f9AEe1603d56732a9c;

        (bool success,) = DEPLOYER.call{value: 1 ether}(new bytes(0));
        require(success);

        // Ensure unique deployment addresses.
        vm.startPrank(DEPLOYER);
        depositContract = IDepositContract(DEPOSIT_CONTRACT);
        target = new BatchDeposit2(DEPOSIT_CONTRACT);
        provider = new DepositData();
        funder = new Funder{value: 1 wei}(payable(target));
        vm.stopPrank();
    }

    function get_deposit_count() private view returns (uint64) {
        bytes memory xs = depositContract.get_deposit_count();
        return (
            (uint64(uint8(xs[0])) << 0x00) | (uint64(uint8(xs[1])) << 0x08) | (uint64(uint8(xs[2])) << 0x10)
                | (uint64(uint8(xs[3])) << 0x18) | (uint64(uint8(xs[4])) << 0x20) | (uint64(uint8(xs[5])) << 0x28)
                | (uint64(uint8(xs[6])) << 0x30) | (uint64(uint8(xs[7])) << 0x38)
        );
    }

    // Ensure the contract works even if funded and has a balance.
    function test_Selfdestruct() public {
        vm.assertEq(address(target).balance, 0);
        funder.fund();
        vm.assertEq(address(target).balance, 1 wei);

        uint256[] memory values = new uint256[](1);
        values[0] = 32 ether;

        bytes memory data = provider.make(0);

        uint256 balanceBefore = DEPOSIT_CONTRACT.balance;
        uint64 depositsBefore = get_deposit_count();
        {
            target.batchDeposit{value: 32 ether}(block.number, values, data);
        }
        uint256 balanceAfter = DEPOSIT_CONTRACT.balance;
        uint64 depositsAfter = get_deposit_count();

        vm.assertEq(balanceAfter, balanceBefore + 32 ether);
        vm.assertEq(depositsAfter, depositsBefore + 1);

        // Ensure the balance of the batch deposit contract remains
        // unchanged.
        vm.assertEq(address(target).balance, 1 wei);
    }
}

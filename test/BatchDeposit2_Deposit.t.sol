// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

import {Test, console} from "forge-std/Test.sol";

import {BatchDeposit2} from "../src/BatchDeposit2.sol";
import {IDepositContract} from "./interfaces/IDepositContract.sol";

import {DepositData} from "./DepositData.sol";

contract BatchDeposit2_Deposit_Test is Test {
    address constant DEPOSIT_CONTRACT = 0x00000000219ab540356cBB839Cbe05303d7705Fa;

    IDepositContract private depositContract;
    BatchDeposit2 private target;
    DepositData private provider;

    function setUp() public {
        // Ensure unique deployment addresses.
        vm.startPrank(0xd380139FFe8a712B7c55F5f9AEe1603d56732a9c);
        depositContract = IDepositContract(DEPOSIT_CONTRACT);
        target = new BatchDeposit2(DEPOSIT_CONTRACT);
        provider = new DepositData();
        vm.stopPrank();
    }

    function get_deposit_count() private view returns (uint64) {
        bytes memory xs = depositContract.get_deposit_count();
        return (
            (uint64(uint8(xs[0])) << 0x00) | (uint64(uint8(xs[1])) << 0x08) | (uint64(uint8(xs[2])) << 0x10)
                | (uint64(uint8(xs[3])) << 0x18) | (uint64(uint8(xs[4])) << 0x20) | (uint64(uint8(xs[5])) << 0x28)
                | (uint64(uint8(xs[6])) << 0x30) | (uint64(uint8(xs[7])) << 0x38)
        );
    }

    // https://github.com/ethereum/consensus-specs/blob/dev/solidity_deposit_contract/deposit_contract.sol#L165
    function to_little_endian_64(uint64 value) internal pure returns (bytes memory ret) {
        ret = new bytes(8);
        bytes8 bytesValue = bytes8(value);
        // Byteswapping during copying to bytes.
        ret[0] = bytesValue[7];
        ret[1] = bytesValue[6];
        ret[2] = bytesValue[5];
        ret[3] = bytesValue[4];
        ret[4] = bytesValue[3];
        ret[5] = bytesValue[2];
        ret[6] = bytesValue[1];
        ret[7] = bytesValue[0];
    }

    function get_next_index(uint64 offset) private view returns (bytes memory ret) {
        return to_little_endian_64(get_deposit_count() + offset);
    }

    function get_value(uint256 value) private pure returns (bytes memory ret) {
        return to_little_endian_64(uint64(value / 1 gwei));
    }

    function test_Deposit_001() public {
        uint256[] memory values = new uint256[](1);
        values[0] = 32 ether;

        bytes memory data = provider.make(0);

        uint256 balanceBefore = DEPOSIT_CONTRACT.balance;
        uint64 depositsBefore = get_deposit_count();
        {
            vm.expectCall(DEPOSIT_CONTRACT, 32 ether, provider.makeCalldata(0));
            vm.expectEmit(DEPOSIT_CONTRACT);
            emit IDepositContract.DepositEvent(
                provider.pubkeys(0),
                provider.withdrawalCredentials(0),
                get_value(32 ether),
                provider.signatures(0),
                get_next_index(0)
            );
            target.batchDeposit{value: 32 ether}(block.number, values, data);
        }
        uint256 balanceAfter = DEPOSIT_CONTRACT.balance;
        uint64 depositsAfter = get_deposit_count();

        vm.assertEq(address(target).balance, 0);
        vm.assertEq(balanceAfter, balanceBefore + 32 ether);
        vm.assertEq(depositsAfter, depositsBefore + 1);
    }

    function test_Deposit_002() public {
        uint256[] memory values = new uint256[](2);
        values[0] = 32 ether;
        values[1] = 32 ether;

        bytes memory data = abi.encodePacked(provider.make(0), provider.make(1));

        uint256 balanceBefore = DEPOSIT_CONTRACT.balance;
        uint64 depositsBefore = get_deposit_count();
        {
            vm.expectCall(DEPOSIT_CONTRACT, 32 ether, provider.makeCalldata(0));
            vm.expectEmit(DEPOSIT_CONTRACT);
            emit IDepositContract.DepositEvent(
                provider.pubkeys(0),
                provider.withdrawalCredentials(0),
                get_value(32 ether),
                provider.signatures(0),
                get_next_index(0)
            );

            vm.expectCall(DEPOSIT_CONTRACT, 32 ether, provider.makeCalldata(1));
            vm.expectEmit(DEPOSIT_CONTRACT);
            emit IDepositContract.DepositEvent(
                provider.pubkeys(1),
                provider.withdrawalCredentials(1),
                get_value(32 ether),
                provider.signatures(1),
                get_next_index(1)
            );

            target.batchDeposit{value: 64 ether}(block.number, values, data);
        }
        uint256 balanceAfter = DEPOSIT_CONTRACT.balance;
        uint64 depositsAfter = get_deposit_count();

        vm.assertEq(address(target).balance, 0);
        vm.assertEq(balanceAfter, balanceBefore + 64 ether);
        vm.assertEq(depositsAfter, depositsBefore + 2);
    }

    function test_Deposit_100() public {
        uint256[] memory values = new uint256[](100);
        bytes memory data;

        for (uint256 i = 0; i < 100; i++) {
            values[i] = 32 ether;
            data = abi.encodePacked(data, provider.make(i));
        }

        uint256 balanceBefore = DEPOSIT_CONTRACT.balance;
        uint64 depositsBefore = get_deposit_count();
        {
            for (uint256 i = 0; i < 100; i++) {
                vm.expectCall(DEPOSIT_CONTRACT, 32 ether, provider.makeCalldata(uint8(i)));
                vm.expectEmit(DEPOSIT_CONTRACT);
                emit IDepositContract.DepositEvent(
                    provider.pubkeys(uint8(i)),
                    provider.withdrawalCredentials(uint8(i)),
                    get_value(32 ether),
                    provider.signatures(uint8(i)),
                    get_next_index(uint64(i))
                );
            }
            target.batchDeposit{value: 3200 ether}(block.number, values, data);
        }
        uint256 balanceAfter = DEPOSIT_CONTRACT.balance;
        uint64 depositsAfter = get_deposit_count();

        vm.assertEq(address(target).balance, 0);
        vm.assertEq(balanceAfter, balanceBefore + 3200 ether);
        vm.assertEq(depositsAfter, depositsBefore + 100);
    }
}

// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

import {console} from "forge-std/console.sol";
import {IDepositContract} from "./interfaces/IDepositContract.sol";

contract DepositContractMock is IDepositContract {
    mapping(uint256 => uint256) public value;
    mapping(uint256 => bytes) public pubkey;
    mapping(uint256 => bytes) public withdrawal_credentials;
    mapping(uint256 => bytes) public signature;
    mapping(uint256 => bytes32) public deposit_data_root;
    uint256 public deposits;
    bool public verbose;

    constructor(bool _verbose) {
        verbose = _verbose;
    }

    function deposit(
        bytes calldata _pubkey,
        bytes calldata _withdrawal_credentials,
        bytes calldata _signature,
        bytes32 _deposit_data_root
    ) external payable override {
        value[deposits] = msg.value;
        pubkey[deposits] = _pubkey;
        withdrawal_credentials[deposits] = _withdrawal_credentials;
        signature[deposits] = _signature;
        deposit_data_root[deposits] = _deposit_data_root;
        deposits += 1;

        if (verbose) {
            console.log("value:", msg.value);
            console.log("pubkey:");
            console.logBytes(_pubkey);
            console.log("withdrawal_credentials:");
            console.logBytes(_withdrawal_credentials);
            console.log("signature:");
            console.logBytes(_signature);
            console.log("deposit_data_root:");
            console.logBytes32(_deposit_data_root);
            console.log();
        }
    }

    function get_deposit_root() external pure override returns (bytes32) {
        return 0;
    }

    function get_deposit_count() external pure override returns (bytes memory) {
        return new bytes(0);
    }
}

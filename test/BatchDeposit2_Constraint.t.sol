// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

import {Test, console} from "forge-std/Test.sol";

import {BatchDeposit2} from "../src/BatchDeposit2.sol";
import {IDepositContract} from "./interfaces/IDepositContract.sol";

import {DepositData} from "./DepositData.sol";

error OutOfFunds();

contract BatchDeposit2_Constraint_Test is Test {
    address constant DEPOSIT_CONTRACT = 0x00000000219ab540356cBB839Cbe05303d7705Fa;

    IDepositContract depositContract;
    BatchDeposit2 private target;
    DepositData private provider;

    function setUp() public {
        // Ensure unique deployment addresses.
        vm.startPrank(0xd380139FFe8a712B7c55F5f9AEe1603d56732a9c);
        depositContract = IDepositContract(DEPOSIT_CONTRACT);
        target = new BatchDeposit2(DEPOSIT_CONTRACT);
        provider = new DepositData();
        vm.stopPrank();
    }

    function test_Constraint_Deadline() public {
        uint256[] memory values = new uint256[](1);
        values[0] = 32 ether;

        bytes memory data;

        data = provider.make(0);
        vm.expectRevert(BatchDeposit2.DeadlineExceeded.selector, address(target));
        target.batchDeposit{value: 32 ether}(block.number - 1, values, data);

        data = provider.make(0);
        target.batchDeposit{value: 32 ether}(block.number, values, data);

        data = provider.make(1);
        target.batchDeposit{value: 32 ether}(block.number + 1, values, data);

        data = provider.make(2);
        target.batchDeposit{value: 32 ether}(block.number + 2, values, data);
    }

    function test_Constraint_Length() public {
        uint256[] memory values = new uint256[](1);
        values[0] = 32 ether;

        bytes memory model = provider.make(0);

        bytes memory one = new bytes(model.length - 1);
        vm.expectRevert(BatchDeposit2.InvalidLength.selector, address(target));
        target.batchDeposit{value: 32 ether}(block.number, values, one);

        bytes memory two = new bytes(0);
        vm.expectRevert(BatchDeposit2.InvalidLength.selector, address(target));
        target.batchDeposit{value: 32 ether}(block.number, values, two);

        bytes memory three = abi.encodePacked(model, model);
        vm.expectRevert(BatchDeposit2.InvalidLength.selector, address(target));
        target.batchDeposit{value: 32 ether}(block.number, values, three);

        bytes memory four = abi.encodePacked(model, bytes1(0));
        vm.expectRevert(BatchDeposit2.InvalidLength.selector, address(target));
        target.batchDeposit{value: 32 ether}(block.number, values, four);
    }

    function test_Constraint_Value_1() public {
        uint256[] memory values = new uint256[](1);
        values[0] = 32 ether;

        bytes memory data = provider.make(0);

        vm.expectRevert(BatchDeposit2.DepositFailed.selector, DEPOSIT_CONTRACT);
        target.batchDeposit{value: 32 ether - 1 wei}(block.number, values, data);

        vm.expectRevert(BatchDeposit2.ExcessiveBalance.selector, address(target));
        target.batchDeposit{value: 32 ether + 1 wei}(block.number, values, data);
    }

    function test_Constraint_Value_2() public {
        uint256[] memory values = new uint256[](1);
        values[0] = 32 ether + 1 wei;

        bytes memory data = provider.make(0);

        // original error is "deposit value not multiple of gwei"
        vm.expectRevert(BatchDeposit2.DepositFailed.selector, DEPOSIT_CONTRACT);
        target.batchDeposit{value: values[0]}(block.number, values, data);
    }

    function test_Constraint_Deposit() public {
        uint256[] memory values = new uint256[](1);
        values[0] = 32 ether;

        bytes memory data = provider.make(0);
        data[0] = 0;

        vm.expectRevert(BatchDeposit2.DepositFailed.selector, DEPOSIT_CONTRACT);
        target.batchDeposit{value: 32 ether}(block.number, values, data);
    }
}

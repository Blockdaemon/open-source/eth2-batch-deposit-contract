// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

import {Test, console} from "forge-std/Test.sol";

import {BatchDeposit2} from "../src/BatchDeposit2.sol";
import {IDepositContract} from "./interfaces/IDepositContract.sol";

import {DepositContractMock} from "./DepositContractMock.sol";
import {DepositDataMock} from "./DepositDataMock.sol";

contract BatchDeposit2_DepositMock_Test is Test {
    DepositContractMock private mockup;
    BatchDeposit2 private target;

    function setUp() public {
        // Ensure unique deployment addresses.
        vm.startPrank(0xd380139FFe8a712B7c55F5f9AEe1603d56732a9c);
        mockup = new DepositContractMock(false);
        target = new BatchDeposit2(address(mockup));
        vm.stopPrank();
    }

    function test_MockDeposit_001() public {
        uint256[] memory values = new uint256[](1);
        values[0] = 14 ether;

        bytes memory data = DepositDataMock.make(17);

        vm.expectCall(address(mockup), 14 ether, DepositDataMock.makeCalldata(17));
        target.batchDeposit{value: 14 ether}(block.number, values, data);

        vm.assertEq(address(mockup).balance, 14 ether);
        vm.assertEq(address(target).balance, 0);

        vm.assertEq(mockup.value(0), 14 ether);
        vm.assertEq(mockup.pubkey(0), DepositDataMock.makePubkey(17));
        vm.assertEq(mockup.withdrawal_credentials(0), DepositDataMock.makeWithdrawalCredentials(17));
        vm.assertEq(mockup.signature(0), DepositDataMock.makeSignature(17));
        vm.assertEq(mockup.deposit_data_root(0), DepositDataMock.makeDepositDataRoot(17));
    }

    function test_MockDeposit_002() public {
        uint256[] memory values = new uint256[](2);
        values[0] = 14 ether;
        values[1] = 65 ether;

        bytes memory data = abi.encodePacked(DepositDataMock.make(17), DepositDataMock.make(39));

        vm.expectCall(address(mockup), 14 ether, DepositDataMock.makeCalldata(17));
        vm.expectCall(address(mockup), 65 ether, DepositDataMock.makeCalldata(39));
        target.batchDeposit{value: 79 ether}(block.number, values, data);

        vm.assertEq(address(mockup).balance, 79 ether);
        vm.assertEq(address(target).balance, 0);

        vm.assertEq(mockup.value(0), 14 ether);
        vm.assertEq(mockup.pubkey(0), DepositDataMock.makePubkey(17));
        vm.assertEq(mockup.withdrawal_credentials(0), DepositDataMock.makeWithdrawalCredentials(17));
        vm.assertEq(mockup.signature(0), DepositDataMock.makeSignature(17));
        vm.assertEq(mockup.deposit_data_root(0), DepositDataMock.makeDepositDataRoot(17));

        vm.assertEq(mockup.value(1), 65 ether);
        vm.assertEq(mockup.pubkey(1), DepositDataMock.makePubkey(39));
        vm.assertEq(mockup.withdrawal_credentials(1), DepositDataMock.makeWithdrawalCredentials(39));
        vm.assertEq(mockup.signature(1), DepositDataMock.makeSignature(39));
        vm.assertEq(mockup.deposit_data_root(1), DepositDataMock.makeDepositDataRoot(39));
    }

    function test_MockDeposit_64() public {
        uint256[] memory values = new uint256[](64);
        bytes memory data;
        for (uint256 i = 0; i < 64; i++) {
            values[i] = (i + 1) * 1 ether;
            data = abi.encodePacked(data, DepositDataMock.make(uint8(i)));
        }

        for (uint256 i = 0; i < 64; i++) {
            vm.expectCall(address(mockup), (i + 1) * 1 ether, DepositDataMock.makeCalldata(uint8(i)));
        }
        target.batchDeposit{value: 2080 ether}(block.number, values, data);

        vm.assertEq(address(mockup).balance, 2080 ether);
        vm.assertEq(address(target).balance, 0);

        for (uint8 i = 0; i < 64; i++) {
            vm.assertEq(mockup.value(i), values[i]);
            vm.assertEq(mockup.pubkey(i), DepositDataMock.makePubkey(i));
            vm.assertEq(mockup.withdrawal_credentials(i), DepositDataMock.makeWithdrawalCredentials(i));
            vm.assertEq(mockup.signature(i), DepositDataMock.makeSignature(i));
            vm.assertEq(mockup.deposit_data_root(i), DepositDataMock.makeDepositDataRoot(i));
        }
    }

    function test_MockDeposit_256() public {
        uint256[] memory values = new uint256[](256);
        bytes memory data;
        for (uint256 i = 0; i < 256; i++) {
            values[i] = (i + 1) * 1 ether;
            data = abi.encodePacked(data, DepositDataMock.make(uint8(i)));
        }

        for (uint256 i = 0; i < 64; i++) {
            vm.expectCall(address(mockup), (i + 1) * 1 ether, DepositDataMock.makeCalldata(uint8(i)));
        }
        target.batchDeposit{value: 32896 ether}(block.number, values, data);

        vm.assertEq(address(mockup).balance, 32896 ether);
        vm.assertEq(address(target).balance, 0);

        for (uint256 x = 0; x < 256; x++) {
            uint8 i = uint8(x);
            vm.assertEq(mockup.value(i), values[i]);
            vm.assertEq(mockup.pubkey(i), DepositDataMock.makePubkey(i));
            vm.assertEq(mockup.withdrawal_credentials(i), DepositDataMock.makeWithdrawalCredentials(i));
            vm.assertEq(mockup.signature(i), DepositDataMock.makeSignature(i));
            vm.assertEq(mockup.deposit_data_root(i), DepositDataMock.makeDepositDataRoot(i));
        }
    }
}

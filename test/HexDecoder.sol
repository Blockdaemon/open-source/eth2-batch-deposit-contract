// SPDX-License-Identifier: GPL3-or-later

pragma solidity 0.8.28;

library HexDecoder {
    function decode(string memory hexString) public pure returns (bytes memory) {
        bytes memory decoded = bytes(hexString);

        require(decoded.length % 2 == 0, "invalid length");

        bytes memory result = new bytes(decoded.length / 2);

        for (uint256 i = 0; i < decoded.length / 2; i++) {
            result[i] = bytes1(16 * hexCharToUint(decoded[2 * i]) + 1 * hexCharToUint(decoded[2 * i + 1]));
        }

        return result;
    }

    function hexCharToUint(bytes1 b) private pure returns (uint8) {
        uint8 x = uint8(b);
        if (0x30 <= x && x <= 0x39) {
            return x - 0x30;
        } else if (0x61 <= x && x <= 0x66) {
            return x - 0x61 + 10;
        } else if (0x41 <= x && x <= 0x46) {
            return x - 0x41 + 10;
        } else {
            revert("invalid hex character");
        }
    }
}
